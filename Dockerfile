FROM nginx:1.13.9-alpine

ADD nginx.conf /etc/nginx/conf.d/default.conf
ADD build/ /srv/www
