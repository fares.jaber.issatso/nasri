import React from 'react';
import cityApi from '../../api/City.api';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import CircularProgress from '@material-ui/core/CircularProgress';

function sleep(delay = 0) {
    return new Promise((resolve) => {
        setTimeout(resolve, delay);
    });
}

export default function Asynchronous() {

    const [open, setOpen] = React.useState(false);
    const [options, setOptions] = React.useState([]);
    const [inputValue, setInputValue] = React.useState('');
    const [loading, setLoading] = React.useState(false);


    React.useEffect(() => {
        let active = true;
        console.log(inputValue)
        if (!inputValue || inputValue.length < 4 || !open) {
            setOptions([])
            return undefined
        }
        (async () => {
            setLoading(true)
            await sleep(1000);
            cityApi.search(inputValue, async (cities) => {
                await sleep(500);
                setLoading(false)
                if (active && cities.data) {
                    setOptions(cities.data);

                }
            });

        })();

        return () => {
            active = false;
        };
    }, [inputValue, open]);


        return(
            <Autocomplete
                id="cities-List"
                style={{ width: 500, color: "white" }}
                open={open}
                onOpen={() => {
                    setOpen(true);
                }}
                onClose={() => {
                    setOpen(false);
                }}
                onChange={(event, selected) => console.log(event, selected)}
                getOptionSelected={(option, value) => option.city === value.city}
                getOptionLabel={(option) => option.city}
                onInputChange={(event, newInputValue) => {
                    setInputValue(newInputValue);
                }}
                options={options}
                renderOption={(option, { selected }) => (
                    <React.Fragment>
                        <img width={40} height={30} src={`https://raw.githubusercontent.com/hjnilsson/country-flags/master/png100px/${option.countryCode.toLowerCase()}.png`} alt={option.countryCode.toLowerCase()}/>
                        &nbsp;{option.city}
                    </React.Fragment>
                )}
                loading={loading}
                renderInput={(params) => (
                    <TextField
                        {...params}
                        label="Enter city"
                        variant="outlined"
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <React.Fragment>
                                    {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                    {params.InputProps.endAdornment}
                                </React.Fragment>
                            ),
                        }}
                    />
                )}
            />)

}
