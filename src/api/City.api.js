
function search(query, cb) {
    const apiKey = '4b3bcf0a5fmsh6151baeab10e796p127425jsnf17d74b09e09'
    const apiHost = 'wft-geo-db.p.rapidapi.com'
    return fetch(`https://wft-geo-db.p.rapidapi.com/v1/geo/cities?namePrefix=${query}`, {
        headers: new Headers({
            accept: "application/json",
            'x-rapidapi-key': apiKey,
            'x-rapidapi-host': apiHost
        })
    })
        .then(checkStatus)
        .then(parseJSON)
        .then(cb);
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    const error = new Error(`HTTP Error ${response.statusText}`);
    error.status = response.statusText;
    error.response = response;
    console.log(error); // eslint-disable-line no-console
    throw error;
}

function parseJSON(response) {
    return response.json();
}


const CityApi = { search };
export default CityApi;