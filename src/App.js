import React from 'react';
import logo from './logo.svg';
import './App.css';
import Search from './components/Search/Search';

class App extends React.Component {
    componentDidMount() {
        navigator.geolocation.getCurrentPosition(function(position) {
            console.log("Latitude is :", position);
            console.log("Longitude is :", position.coords.longitude);
        });
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <Search/>
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                </header>
            </div>
        );
    }

}

export default App;
